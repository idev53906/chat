package fr.jayrex.chat.managers;

import fr.jayrex.chat.ChatPlugin;
import fr.jayrex.chat.sql.DbAccess;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public class UserManager {
    private final ChatPlugin plugin;

    public UserManager(ChatPlugin plugin) {
        this.plugin = plugin;
    }

    public void setUserChannel(UUID user, String channel) {
        try (Connection conn = DbAccess.getDataSource().getConnection();
             PreparedStatement ps = conn.prepareStatement("REPLACE INTO user_channels (uuid, channel) VALUES (?, ?)")) {
            ps.setString(1, user.toString());
            ps.setString(2, channel);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public String getUserChannel(UUID user) {
        try (Connection conn = DbAccess.getDataSource().getConnection();
             PreparedStatement ps = conn.prepareStatement("SELECT channel FROM user_channels WHERE uuid = ?")) {
            ps.setString(1, user.toString());
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getString("channel");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return "global";
    }

    public void setProfanityFilter(UUID user, boolean enabled) {
        try (Connection conn = DbAccess.getDataSource().getConnection();
             PreparedStatement ps = conn.prepareStatement("REPLACE INTO user_preferences (uuid, profanity_filter) VALUES (?, ?)")) {
            ps.setString(1, user.toString());
            ps.setBoolean(2, enabled);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean getProfanityFilter(UUID user) {
        try (Connection conn = DbAccess.getDataSource().getConnection();
             PreparedStatement ps = conn.prepareStatement("SELECT profanity_filter FROM user_preferences WHERE uuid = ?")) {
            ps.setString(1, user.toString());
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getBoolean("profanity_filter");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}
