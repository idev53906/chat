package fr.jayrex.chat.managers;

import fr.jayrex.chat.ChatPlugin;
import fr.jayrex.chat.models.ChatChannel;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.HashMap;
import java.util.Map;

public class ChannelManager {
    private final ChatPlugin plugin;
    private final Map<String, ChatChannel> channels = new HashMap<>();

    public ChannelManager(ChatPlugin plugin) {
        this.plugin = plugin;
    }

    public void loadChannels() {
        FileConfiguration config = plugin.getConfig();
        config.getConfigurationSection("channels").getKeys(false).forEach(key -> {
            String prefix = config.getString("channels." + key + ".prefix");
            boolean isGlobal = config.getBoolean("channels." + key + ".global");
            int range = config.getInt("channels." + key + ".range");
            channels.put(key, new ChatChannel(key, prefix, isGlobal, range));
        });
    }

    public ChatChannel getChannel(String name) {
        return channels.get(name);
    }

    public boolean createChannel(String name, String prefix, boolean isGlobal, int range) {
        if (channels.containsKey(name)) {
            return false;
        }
        channels.put(name, new ChatChannel(name, prefix, isGlobal, range));
        saveChannelToConfig(name, prefix, isGlobal, range);
        return true;
    }

    public boolean deleteChannel(String name) {
        if (channels.remove(name) != null) {
            plugin.getConfig().set("channels." + name, null);
            plugin.saveConfig();
            return true;
        }
        return false;
    }

    private void saveChannelToConfig(String name, String prefix, boolean isGlobal, int range) {
        String path = "channels." + name;
        plugin.getConfig().set(path + ".prefix", prefix);
        plugin.getConfig().set(path + ".global", isGlobal);
        plugin.getConfig().set(path + ".range", range);
        plugin.saveConfig();
    }
}
