package fr.jayrex.chat.commands;

import fr.jayrex.chat.ChatPlugin;
import fr.jayrex.chat.managers.ChannelManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.jetbrains.annotations.NotNull;

public class ChatCommand implements CommandExecutor {
    private final ChatPlugin plugin;

    public ChatCommand(ChatPlugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        if (args.length < 1) {
            return false;
        }

        ChannelManager channelManager = plugin.getChannelManager();
        switch (args[0].toLowerCase()) {
            case "create":
                if (args.length < 5) return false; // create [name] [prefix] [global|local] [range]
                boolean isGlobal = args[3].equalsIgnoreCase("global");
                int range;
                try {
                    range = Integer.parseInt(args[4]);
                } catch (NumberFormatException e) {
                    sender.sendMessage("Invalid number for range.");
                    return true;
                }
                if (channelManager.createChannel(args[1], args[2], isGlobal, range)) {
                    sender.sendMessage("Channel created successfully.");
                } else {
                    sender.sendMessage("Channel creation failed. It may already exist.");
                }
                break;
            case "delete":
                if (args.length < 2) return false;
                if (channelManager.deleteChannel(args[1])) {
                    sender.sendMessage("Channel deleted successfully.");
                } else {
                    sender.sendMessage("Channel deletion failed. It may not exist.");
                }
                break;
            default:
                return false;
        }
        return true;
    }
}
