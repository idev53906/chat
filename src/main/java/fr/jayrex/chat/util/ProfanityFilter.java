package fr.jayrex.chat.util;

import fr.jayrex.chat.ChatPlugin;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

public class ProfanityFilter {
    private final Set<String> badWords;

    public ProfanityFilter(ChatPlugin plugin) {
        this.badWords = new HashSet<>();
        loadBadWords(plugin);
    }

    private void loadBadWords(ChatPlugin plugin) {
        File wordsFile = new File(plugin.getDataFolder(), "words.yml");
        if (!wordsFile.exists()) {
            plugin.saveResource("words.yml", false);
        }
        FileConfiguration wordsConfig = YamlConfiguration.loadConfiguration(wordsFile);
        badWords.addAll(wordsConfig.getStringList("badWords"));
    }

    public boolean containsProfanity(String message) {
        for (String word : message.split("\\s+")) {
            if (badWords.contains(word.toLowerCase())) {
                return true;
            }
        }
        return false;
    }

    public String filterText(String message) {
        for (String word : message.split("\\s+")) {
            if (badWords.contains(word.toLowerCase())) {
                message = message.replaceAll("\\b" + word + "\\b", "*".repeat(word.length()));
            }
        }
        return message;
    }
}