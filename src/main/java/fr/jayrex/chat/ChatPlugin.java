package fr.jayrex.chat;

import fr.jayrex.chat.commands.ChatCommand;
import fr.jayrex.chat.listeners.ChatListener;
import org.bukkit.plugin.java.JavaPlugin;
import fr.jayrex.chat.managers.ChannelManager;
import fr.jayrex.chat.managers.UserManager;
import fr.jayrex.chat.sql.DbAccess;

public final class ChatPlugin extends JavaPlugin {
    private ChannelManager channelManager;
    private UserManager userManager;

    @Override
    public void onEnable() {
        saveDefaultConfig();
        this.channelManager = new ChannelManager(this);
        this.userManager = new UserManager(this);

        String host = getConfig().getString("database.host");
        int port = getConfig().getInt("database.port");
        String dbname = getConfig().getString("database.dbname");
        String user = getConfig().getString("database.user");
        String password = getConfig().getString("database.password");
        DbAccess.initPool(host, port, dbname, user, password);

        channelManager.loadChannels();

        getCommand("chat").setExecutor(new ChatCommand(this));
        getServer().getPluginManager().registerEvents(new ChatListener(this), this);
    }

    @Override
    public void onDisable() {
        DbAccess.closePool();
    }
    public ChannelManager getChannelManager() {
        return channelManager;
    }

    public UserManager getUserManager() {
        return userManager;
    }
}
