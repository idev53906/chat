package fr.jayrex.chat.models;

public class ChatChannel {
    private final String name;
    private final String prefix;
    private final boolean isGlobal;
    private final int range;

    public ChatChannel(String name, String prefix, boolean isGlobal, int range) {
        this.name = name;
        this.prefix = prefix;
        this.isGlobal = isGlobal;
        this.range = range;
    }

    public String getName() {
        return name;
    }

    public String getPrefix() {
        return prefix;
    }

    public boolean isGlobal() {
        return isGlobal;
    }

    public int getRange() {
        return range;
    }
}
