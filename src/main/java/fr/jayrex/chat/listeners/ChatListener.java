package fr.jayrex.chat.listeners;

import fr.jayrex.chat.ChatPlugin;
import fr.jayrex.chat.models.ChatChannel;
import fr.jayrex.chat.util.ProfanityFilter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class ChatListener implements Listener {
    private final ChatPlugin plugin;
    private final ProfanityFilter filter;

    public ChatListener(ChatPlugin plugin) {
        this.plugin = plugin;
        this.filter = new ProfanityFilter(plugin);
    }

    @EventHandler
    public void onPlayerChat(AsyncPlayerChatEvent event) {
        Player player = event.getPlayer();
        String message = event.getMessage();
        String filteredMessage = message;

        if (plugin.getUserManager().getProfanityFilter(player.getUniqueId())) {
            filteredMessage = filter.filterText(message);
        }

        String channelName = plugin.getChannelManager().getChannel(player.getWorld().getName()).getName();
        ChatChannel channel = plugin.getChannelManager().getChannel(channelName);

        if (channel.isGlobal()) {
            event.setFormat(channel.getPrefix() + "<%1$s> %2$s");
            event.setMessage(filteredMessage);
        } else {
            event.setCancelled(true);
            double range = channel.getRange();
            final String finalMessage = filteredMessage;
            Bukkit.getOnlinePlayers().stream()
                    .filter(p -> p.getWorld().equals(player.getWorld()))
                    .filter(p -> p.getLocation().distance(player.getLocation()) <= range)
                    .forEach(p -> p.sendMessage(channel.getPrefix() + "<" + player.getName() + "> " + finalMessage));
        }
    }
}
